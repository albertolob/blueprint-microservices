package io.simplematter.microservices.order.processor.projector;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.order.protocol.serdes.EventSerdes;
import io.simplematter.microservices.order.protocol.serdes.SnapshotSerdes;
import io.simplematter.microservices.order.StaticConfigs;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.OrderSnapshot;
import io.simplematter.microservices.order.protocol.internal.snapshot.Order;
import io.simplematter.microservices.common.projection.Store;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class OrderProjector {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Properties config;
    private final Store<String, Order> store;
    private KafkaStreams streams;

    public OrderProjector(final Properties config, final Store<String, Order> store) {

        Objects.requireNonNull(config.getProperty("application.id"));
        Objects.requireNonNull(config.getProperty("schema.registry.url"));
        this.config = config;
        this.store = store;
    }

    public void start() {

        logger.info("Starting " + getClass().getSimpleName());

        final String applicationId = config.getProperty("application.id");
        final String registryUrl = config.getProperty("schema.registry.url");

        logger.info("Registering applicationId: " + applicationId);

        final SchemaRegistryClient schemaRegistry = new CachedSchemaRegistryClient(registryUrl, 100);

        final Serde<OrderEvent> eventSerde = new EventSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();
        final Serde<OrderSnapshot> snapshotSerde = new SnapshotSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();

        final KStreamBuilder builder = new KStreamBuilder();
        // builder.newName(CheckoutProjector.class.getSimpleName() + "_" + INDEX);

        final KStream<String, OrderEvent> events = builder.stream(new Serdes.StringSerde(),
                eventSerde, StaticConfigs.EVENT_TOPIC);

        final KStream<String, OrderSnapshot> snapshots = builder.stream(new Serdes.StringSerde(),
                snapshotSerde, StaticConfigs.SNAPSHOT_TOPIC);

        final KStream<String, OrderSnapshot> results = events.join(snapshots, (event, snapshot) -> {
                    OrderSnapshot result = null;
                    if (event.getPayload().getId().equals(snapshot.getPayload().getEventId())) {
                        result = snapshot;
                    }
                    return result;
                }, JoinWindows.of(TimeUnit.SECONDS.toMillis(10)),
                new Serdes.StringSerde(), eventSerde, snapshotSerde).filter((k, v) -> v != null);


        results.foreach((k, v) -> {
            store.persist((Order) v.getPayload());
        });

        streams = new KafkaStreams(builder, config);

        streams.start();

        logger.info(getClass().getSimpleName() + " Started");
    }

    public void stop() {

        try {
            logger.info("Stopping " + getClass().getSimpleName());
            streams.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info(getClass().getSimpleName() + " Stopped");
        }
    }
}

/*
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.projection.Store;
import io.simplematter.microservices.order.StaticConfigs;
import io.simplematter.microservices.order.protocol.internal.OrderEvent;
import io.simplematter.microservices.order.protocol.internal.OrderSnapshot;
import io.simplematter.microservices.order.protocol.internal.snapshot.Order;
import io.simplematter.microservices.order.protocol.serdes.EventSerdes;
import io.simplematter.microservices.order.protocol.serdes.SnapshotSerdes;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
*/
/*
public class OrderProjector extends AbstractVerticle {

    private KafkaStreams streams;

    public Properties getProperties(final JsonObject config) {

        final Properties props = new Properties();
        props.put("bootstrap.servers", config.getString("bootstrap.servers"));
        //props.put("zookeeper.connect", "localhost:2181")
        props.put("application.id", config.getString("application.id"));
        props.put("auto.create.topics.enable", config.getString("auto.create.topics.enable"));
        //props.put("group.id", "orderinterface.client.2");
        props.put("linger.ms", config.getString("linger.ms"));
        props.put("timeout.ms", config.getString("timeout.ms"));
        props.put("batch.size", config.getString("batch.size"));
        props.put("buffer.memory", config.getString("buffer.memory"));
        props.put("retries", config.getString("retries"));
        props.put("auto.commit.enable", config.getString("auto.commit.enable"));
        props.put("schema.registry.url", config.getString("schema.registry.url"));
        return props;
    }

    public void start(final Future<Void> future) {

        final Properties props = getProperties(config());

        final String applicationId = config().getString("application.id");
        final String registryUrl = config().getString("schema.registry.url");

        final SchemaRegistryClient schemaRegistry = new CachedSchemaRegistryClient(registryUrl, 100);

        final Serde<OrderEvent> eventSerde = new EventSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();
        final Serde<OrderSnapshot> snapshotSerde = new SnapshotSerdes(applicationId, schemaRegistry, registryUrl).getSerdes();

        final KStreamBuilder builder = new KStreamBuilder();
        //builder.newName(OrderProjector.class.getSimpleName() + "_" + INDEX);

        final KStream<String, OrderEvent> events = builder.stream(new Serdes.StringSerde(),
                eventSerde, StaticConfigs.EVENT_TOPIC);

        final KStream<String, OrderSnapshot> snapshots = builder.stream(new Serdes.StringSerde(),
                snapshotSerde, StaticConfigs.SNAPSHOT_TOPIC);

        //final RethinkDBConnector connector = new RethinkDBConnector();
        final Store<Order> store = new Store<Order>() {

            @Override
            public void write(Order value) {

            }

            @Override
            public void write(List<Order> values) {

            }
        };

        final KStream<String, Optional<OrderSnapshot>> results = events.join(snapshots, (event, snapshot) -> {
                    Optional<OrderSnapshot> result = null;
                    if (event.getId().equals(snapshot.getPayload().getEventId())) {
                        result = Optional.of(snapshot);
                    } else {
                        result = Optional.empty();
                    }
                    return result;
                }, JoinWindows.of(TimeUnit.SECONDS.toMillis(5)),
                new Serdes.StringSerde(), eventSerde, snapshotSerde).filter((k, v) -> v.isPresent());


        results.foreach((k, v) -> {
            store.write((Order) v.get().getPayload());
            System.out.println("CORRELATION " + v.get());
        });

        streams = new KafkaStreams(builder, props);

        streams.start();

        future.complete();
    }

    @Override
    public void stop(final Future<Void> future) {

        try {
            streams.close();
            future.complete();
        } catch (Exception e) {
            future.fail(e.getCause());
        }
    }

    public static void main(String[] args) {

        final String applicationId = OrderProjector.class.getSimpleName().toUpperCase() + "-" + StaticConfigs.INDEX;

        final JsonObject config = new JsonObject();
        config.put("bootstrap.servers", "PLAINTEXT://127.0.0.1:9092");
        //props.put("zookeeper.connect", "localhost:2181")
        config.put("application.id", applicationId);
        config.put("auto.create.topics.enable", "true");
        //props.put("group.id", "orderinterface.client.2");
        config.put("linger.ms", "0");
        config.put("timeout.ms", "3000");
        config.put("batch.size", "16384");
        config.put("buffer.memory", "33554432");
        config.put("retries", "1");
        config.put("auto.commit.enable", "false");
        config.put("schema.registry.url", "http://localhost:8081");

        final DeploymentOptions options = new DeploymentOptions()
                .setConfig(config);

        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new OrderProjector(), options);

    }
} */

/*
                    if (x.get("id").asText().equals(y.get("eventId").asText())) {
                        System.out.println("Not Empty");
                        return Optional.of(y);
                    } else {
                        System.out.println("Empty");
                        return Optional.empty();
                    }

 */