package io.simplematter.microservices.checkout.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.checkout.protocol.avro.CheckoutStateAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutState;
import io.simplematter.microservices.checkout.protocol.converter.StateConverter;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class StateSerdes extends SerdesSupplier<CheckoutState> {

    public StateSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<CheckoutState> getSerializer() {

        final AvroSerializer<CheckoutState> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new StateConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<CheckoutState> getDeserializer() {

        final AvroDeserializer<CheckoutState> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new StateConverter(), CheckoutStateAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<CheckoutState> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
