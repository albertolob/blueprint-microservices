package io.simplematter.microservices.checkout.protocol.internal.command;

import io.simplematter.microservices.common.protocol.Command;

public class RejectCheckout implements Command<String> {

    private String id;
    private String reason;
    private String orderId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }
}
