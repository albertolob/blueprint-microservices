package io.simplematter.microservices.checkout.protocol.internal.state;

import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutAcknowledged;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutConfirmed;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutRejected;
import io.simplematter.microservices.checkout.protocol.internal.event.CheckoutRequested;
import io.simplematter.microservices.common.protocol.State;

public class Checkout implements State<String> {

    public static final String REQUESTED = "REQUESTED";
    public static final String ACKNOWLEDGED = "ACKNOWLEDGED";
    public static final String CONFIRMED = "CONFIRMED";
    public static final String REJECTED = "REJECTED";

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private String orderId;
    private String paymentId;
    private String status;
    private String eventId;
    private long offset;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getStatus() {
        return status;
    }

    public String getEventId() {
        return eventId;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    public static Checkout create(final CheckoutRequested event, final long offset) {

        final Checkout state = new Checkout();
        state.setId(event.getId());
        state.setSku(event.getSku());
        state.setQuantity(event.getQuantity());
        state.setPrice(event.getPrice());
        state.setCustomerId(event.getCustomerId());
        state.setStatus(Checkout.REQUESTED);
        state.setEventId(event.getId());
        state.setOffset(offset);
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    public Checkout update(final CheckoutAcknowledged event, final long offset) {

        setOrderId(event.getOrderId());
        setStatus(Checkout.ACKNOWLEDGED);
        setEventId(event.getId());
        setOffset(offset);
        setTimestamp(System.currentTimeMillis());
        return this;
    }

    public Checkout update(final CheckoutConfirmed event, final long offset) {

        setPaymentId(event.getPaymentId());
        setStatus(Checkout.CONFIRMED);
        setEventId(event.getId());
        setOffset(offset);
        setTimestamp(System.currentTimeMillis());
        return this;
    }

    public Checkout update(final CheckoutRejected event, final long offset) {

        setStatus(Checkout.REJECTED);
        setEventId(event.getId());
        setOffset(offset);
        setTimestamp(System.currentTimeMillis());
        return this;
    }
}
