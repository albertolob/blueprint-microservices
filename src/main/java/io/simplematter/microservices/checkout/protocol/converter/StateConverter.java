package io.simplematter.microservices.checkout.protocol.converter;

import io.simplematter.microservices.checkout.protocol.avro.CheckoutStateAvro;
import io.simplematter.microservices.checkout.protocol.avro.state.CheckoutAvro;
import io.simplematter.microservices.checkout.protocol.internal.CheckoutState;
import io.simplematter.microservices.checkout.protocol.internal.state.Checkout;
import io.simplematter.microservices.common.protocol.Converter;

public class StateConverter implements Converter<CheckoutState, CheckoutStateAvro> {

    @Override
    public CheckoutStateAvro serialize(final CheckoutState source) {

        final CheckoutStateAvro sink = new CheckoutStateAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setTimeout(source.getTimeout());
        sink.setManifest(source.getManifest());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof Checkout) {
            sink.setPayload(serialize((Checkout)
                    source.getPayload()));
        }
        return sink;
    }

    @Override
    public CheckoutState deserialize(final CheckoutStateAvro source) {

        final CheckoutState sink = new CheckoutState();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() != null) {
            sink.setPayload(deserialize(source.getPayload()));
        }
        return sink;
    }

    private CheckoutAvro serialize(final Checkout source) {

        final CheckoutAvro sink = new CheckoutAvro();
        sink.setId(source.getId());
        sink.setSku(source.getSku());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId());
        sink.setOrderId(source.getOrderId());
        sink.setPaymentId(source.getPaymentId());
        sink.setStatus(source.getStatus());
        sink.setEventId(source.getEventId());
        sink.setOffset(source.getOffset());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private Checkout deserialize(final CheckoutAvro source) {

        final Checkout sink = new Checkout();
        sink.setId(source.getId().toString());
        sink.setSku(source.getSku().toString());
        sink.setQuantity(source.getQuantity());
        sink.setPrice(source.getPrice());
        sink.setCustomerId(source.getCustomerId().toString());
        sink.setOrderId(source.getOrderId() == null ? null : source.getOrderId().toString());
        sink.setPaymentId(source.getPaymentId() == null ? null : source.getPaymentId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setOffset(source.getOffset());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
