package io.simplematter.microservices.payment.vertx;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import io.simplematter.microservices.common.stream.AbstractJob;
import io.simplematter.microservices.payment.StaticConfigs;
import io.simplematter.microservices.payment.processor.ProcessorRunner;
import io.simplematter.microservices.payment.processor.adapter.AdapterRunner;
import io.simplematter.microservices.payment.processor.projector.ProjectorRunner;
import io.simplematter.microservices.payment.processor.projector.ProjectorService;
import io.simplematter.microservices.payment.processor.projector.ProjectorStore;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class StartupVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final List<AbstractJob> processors;
    private List<String> deployments;

    public StartupVerticle() {

        this.processors = new ArrayList<>();
        this.deployments = new ArrayList<>();
    }

    private Properties getProperties(final JsonObject config) {

        final Properties props = new Properties();
        props.put("bootstrap.servers", config.getString("bootstrap.servers"));
        //props.put("zookeeper.connect", "localhost:2181")
        props.put("application.id", config.getString("application.id"));
        props.put("auto.create.topics.enable", config.getString("auto.create.topics.enable"));
        //props.put("group.id", "orderinterface.client.2");
        props.put("linger.ms", config.getString("linger.ms"));
        props.put("timeout.ms", config.getString("timeout.ms"));
        props.put("batch.size", config.getString("batch.size"));
        props.put("buffer.memory", config.getString("buffer.memory"));
        props.put("retries", config.getString("retries"));
        props.put("auto.commit.enable", config.getString("auto.commit.enable"));
        props.put("schema.registry.url", config.getString("schema.registry.url"));
        return props;
    }

    @Override
    public void start(final Future<Void> future) {

        /*
        final Properties properties = getProperties(config());

        final DeploymentOptions deploymentOptions = new DeploymentOptions()
                .setWorker(true)
                //.setInstances(20)
                .setWorkerPoolSize(20);
                */

        final ConfigStoreOptions configStore = new ConfigStoreOptions()
                .setType("file")
                .setFormat("hocon")
                .setConfig(new JsonObject()
                        .put("path", "payment_service.conf")
                );

        final ConfigRetrieverOptions options = new ConfigRetrieverOptions();
        options.addStore(configStore);

        /*
        final ConfigRetriever retriever = ConfigRetriever.create(vertx,
                new ConfigRetrieverOptions().addStore(configStore)); */

        ConfigRetriever.create(vertx, options).getConfig(handler -> {
            if (handler.succeeded()) {

                final JsonObject config = handler.result();

                final Properties adapterConfig = getProperties(config.getJsonObject("service").
                        getJsonObject("adapter").getJsonObject("kafka"));
                processors.add(new AdapterRunner(adapterConfig));

                final Properties processorConfig = getProperties(config.getJsonObject("service").
                        getJsonObject("processor").getJsonObject("kafka"));
                processors.add(new ProcessorRunner(processorConfig));

                final JsonObject storeConfig = config.getJsonObject("service").getJsonObject("projector").getJsonObject("store");
                final Connection connection = RethinkDB.r.connection().hostname(storeConfig.getString("host"))
                        .port(storeConfig.getInteger("port")).connect();
                final String table = storeConfig.getString("table");
                final Properties projectorConfig = getProperties(config.getJsonObject("service").
                        getJsonObject("projector").getJsonObject("kafka"));
                final ProjectorStore projectorStore = new ProjectorStore(connection, table);
                processors.add(new ProjectorRunner(projectorConfig, projectorStore));

                processors.forEach(processor -> {
                    try {
                        processor.start();
                    } catch (Exception e) {
                        logger.error(e);
                    }
                });

                final ProjectorService projectorService = new ProjectorService(projectorStore);
                vertx.deployVerticle(new ProjectorVerticle(projectorService), new DeploymentOptions()
                        .setConfig(new JsonObject()
                                .put("service.http.host", config.getJsonObject("service").getJsonObject("http").getString("host"))
                                .put("service.http.port", config.getJsonObject("service").getJsonObject("http").getInteger("port")))
                        .setWorker(true), deploymentResult -> {
                    if (deploymentResult.succeeded()) {
                        future.complete();
                    } else {
                        future.fail(deploymentResult.cause());
                    }
                });

            } else {
                future.fail(handler.cause());
            }
        });
    }

    @Override
    public void stop(final Future<Void> future) {

        processors.forEach(processor -> {
            try {
                processor.stop();
            } catch (Exception e) {
                logger.error(e);
            }
        });

        deployments.forEach(deploymentId -> {
            vertx.undeploy(deploymentId, handler -> {
                if (handler.succeeded()) {
                    deployments.remove(deploymentId);
                    logger.info(deploymentId + " successfully undeployed");
                } else {
                    logger.error(handler.cause());
                }
            });
        });

        future.complete();

    }

    public static void main(String args[]) {


        final String applicationId = StaticConfigs.APPLICATION_ID;

        final JsonObject config = new JsonObject();
        config.put("bootstrap.servers", "PLAINTEXT://127.0.0.1:9092");
        //props.put("zookeeper.connect", "localhost:2181")
        config.put("application.id", applicationId);
        config.put("auto.create.topics.enable", "true");
        //props.put("group.id", "orderinterface.client.2");
        config.put("linger.ms", "0");
        config.put("timeout.ms", "3000");
        config.put("batch.size", "16384");
        config.put("buffer.memory", "33554432");
        config.put("retries", "1");
        config.put("auto.commit.enable", "false");
        config.put("schema.registry.url", "http://localhost:8081");

        final DeploymentOptions options = new DeploymentOptions()
                .setConfig(config);

        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new StartupVerticle(), options);
    }

}
