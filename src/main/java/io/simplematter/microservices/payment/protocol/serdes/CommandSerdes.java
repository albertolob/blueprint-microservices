package io.simplematter.microservices.payment.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.payment.protocol.converter.CommandConverter;
import io.simplematter.microservices.payment.protocol.avro.PaymentCommandAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentCommand;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class CommandSerdes extends SerdesSupplier<PaymentCommand> {

    public CommandSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<PaymentCommand> getSerializer() {

        final AvroSerializer<PaymentCommand> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new CommandConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<PaymentCommand> getDeserializer() {

        final AvroDeserializer<PaymentCommand> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new CommandConverter(), PaymentCommandAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<PaymentCommand> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
