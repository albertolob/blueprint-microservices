package io.simplematter.microservices.payment.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.payment.protocol.converter.EventConverter;
import io.simplematter.microservices.payment.protocol.avro.PaymentEventAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentEvent;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class EventSerdes extends SerdesSupplier<PaymentEvent> {

    public EventSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<PaymentEvent> getSerializer() {

        final AvroSerializer<PaymentEvent> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new EventConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<PaymentEvent> getDeserializer() {

        final AvroDeserializer<PaymentEvent> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new EventConverter(), PaymentEventAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<PaymentEvent> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
