package io.simplematter.microservices.payment.protocol.converter;

import io.simplematter.microservices.common.protocol.Converter;
import io.simplematter.microservices.payment.protocol.avro.PaymentStateAvro;
import io.simplematter.microservices.payment.protocol.avro.state.PaymentAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentState;
import io.simplematter.microservices.payment.protocol.internal.state.Payment;

public class StateConverter implements Converter<PaymentState, PaymentStateAvro> {

    @Override
    public PaymentStateAvro serialize(final PaymentState source) {

        final PaymentStateAvro sink = new PaymentStateAvro();
        sink.setId(source.getId());
        sink.setCorrelationId(source.getCorrelationId());
        sink.setManifest(source.getManifest());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() instanceof Payment) {
            sink.setPayload(serialize((Payment) source.getPayload()));
        }
        return sink;
    }

    @Override
    public PaymentState deserialize(final PaymentStateAvro source) {

        final PaymentState sink = new PaymentState();
        sink.setId(source.getId().toString());
        sink.setCorrelationId(source.getCorrelationId().toString());
        sink.setTimeout(source.getTimeout());
        sink.setTimestamp(source.getTimestamp());
        if (source.getPayload() != null) {
            sink.setPayload(deserialize(source.getPayload()));
        }
        return sink;
    }

    private PaymentAvro serialize(final Payment source) {

        final PaymentAvro sink = new PaymentAvro();
        sink.setId(source.getId());
        sink.setCardHolder(source.getCardHolder());
        sink.setCardType(source.getCardType());
        sink.setCardNumber(source.getCardNumber());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency());
        sink.setOrderId(source.getOrderId());
        sink.setStatus(source.getStatus());
        sink.setEventId(source.getEventId());
        sink.setOffset(source.getOffset());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }

    private Payment deserialize(final PaymentAvro source) {

        final Payment sink = new Payment();
        sink.setId(source.getId().toString());
        sink.setCardHolder(source.getCardHolder().toString());
        sink.setCardType(source.getCardType().toString());
        sink.setCardNumber(source.getCardNumber().toString());
        sink.setAmount(source.getAmount());
        sink.setCurrency(source.getCurrency().toString());
        sink.setOrderId(source.getOrderId().toString());
        sink.setStatus(source.getStatus().toString());
        sink.setEventId(source.getEventId().toString());
        sink.setOffset(source.getOffset());
        sink.setTimestamp(source.getTimestamp());
        return sink;
    }
}
