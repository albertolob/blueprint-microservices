package io.simplematter.microservices.payment.protocol.serdes;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.simplematter.microservices.common.protocol.serdes.SerdesSupplier;
import io.simplematter.microservices.common.kafka.serialization.AvroDeserializer;
import io.simplematter.microservices.common.kafka.serialization.AvroSerializer;
import io.simplematter.microservices.payment.protocol.converter.SnapshotConverter;
import io.simplematter.microservices.payment.protocol.avro.PaymentSnapshotAvro;
import io.simplematter.microservices.payment.protocol.internal.PaymentSnapshot;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;

public class SnapshotSerdes extends SerdesSupplier<PaymentSnapshot> {

    public SnapshotSerdes(final String applicationId, final SchemaRegistryClient schemaRegistry, final String registryUrl) {

        super(applicationId, schemaRegistry, registryUrl);
    }

    @Override
    public AvroSerializer<PaymentSnapshot> getSerializer() {

        final AvroSerializer<PaymentSnapshot> serializer = new AvroSerializer(getApplicationId(), getSchemaRegistry(), new SnapshotConverter());

        serializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return serializer;
    }

    @Override
    public AvroDeserializer<PaymentSnapshot> getDeserializer() {

        final AvroDeserializer<PaymentSnapshot> deserializer = new AvroDeserializer(getApplicationId(), getSchemaRegistry(), new SnapshotConverter(), PaymentSnapshotAvro.SCHEMA$);

        deserializer.configure(new HashMap<String, String>() {
            {
                put("specific.avro.reader", "true");
            }

            {
                put("schema.registry.url", getRegistryUrl());
            }
        }, false);

        return deserializer;
    }

    @Override
    public Serde<PaymentSnapshot> getSerdes() {

        return Serdes.serdeFrom(getSerializer(), getDeserializer());
    }

}
