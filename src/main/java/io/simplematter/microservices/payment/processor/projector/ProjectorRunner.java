package io.simplematter.microservices.payment.processor.projector;

import io.simplematter.microservices.common.projection.Store;
import io.simplematter.microservices.common.stream.AbstractJob;
import io.simplematter.microservices.payment.protocol.internal.snapshot.Payment;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Properties;

public class ProjectorRunner extends AbstractJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final PaymentProjector projector;

    public ProjectorRunner(final Properties properties, final Store<String, Payment> store) {

        super(properties);

        projector = new PaymentProjector(getProperties(), store);
    }

    @Override
    public void start() {

        projector.start();
    }

    @Override
    public void stop() {

        projector.stop();
    }
}
