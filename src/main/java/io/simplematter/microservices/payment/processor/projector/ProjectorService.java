package io.simplematter.microservices.payment.processor.projector;

import io.simplematter.microservices.payment.protocol.internal.snapshot.Payment;
import io.vertx.core.Handler;

import java.util.List;

public class ProjectorService {

    //private CircuitBreaker circuitBreaker;
    private final ProjectorStore store;

    public ProjectorService(final ProjectorStore store) {

        this.store = store;
    }

    public void byId(final String id, final Handler<Payment> handler) {

        handler.handle(store.byId(id));
    }

    public void byIds(final List<String> ids, final Handler<List<Payment>> handler) {

        handler.handle(store.byIds(ids));
    }

    public void all(final int limit, final Handler<List<Payment>> handler) {

        handler.handle(store.all(limit));
    }
}
